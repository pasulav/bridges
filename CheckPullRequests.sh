#!/bin/bash
#--------------------------------------------------------------------------------------------------
# Script to Check and send email if there are Merge Conflicts and Auto Merge failures in Bitbucket Pull Requests
#
# Version       Date           Author                 Comments
# -------       -----          ------                 ---------
#  1          04/16/2020       Venkat Pasula          Initial Version 
#--------------------------------------------------------------------------------------------------
finished='no'
JsonStartPos=0

Uid=''
CheckPullRequestsOutputFile=''
StopCheckPullRequestFile=''
ScriptRunlog=''

BaseDir=''
CherrypickUser=''
CherrypickEncPass=''
BitbucketUrl=''
BitBucketProject=''
BitBucketRepo=''

PropsDir=''
DependencyPropsFile=''
BuildTeamEmailIds=mathiazhagand@michigan.gov,eskewc@michigan.gov,pasulav@michigan.gov

function Send_Email {

   local InPullReqno=${1}
   local InPullReqAuthr=${2} 
   local InPullReqFrom=${3}
   local InPullReqTo=${4}
   local InToEmail=${5}
   local InCCEmail=${6}
   local InEmailMessage=${7} 
   local InPullReqCreateDt=${8}
   local InPullReqPrtcpts=${9}
   
   echo -e "${InPullReqAuthr},\n ${InEmailMessage}. See below for more details on the Pull Request\n\n  \t Pull Request #              : ${InPullReqno} \n \t Pull Request from           : ${InPullReqFrom} \n \t Pull Request To             : ${InPullReqTo} \n \t Pull Request Create Date    : ${InPullReqCreateDt}\n \t Pull Request Participants   : ${InPullReqPrtcpts} \n\n Please resolve the issue as soon as possible." | mailx -s "ACTION REQUIRED : ${InEmailMessage}" -c ${InCCEmail},${BuildTeamEmailIds} ${InToEmail} 
   
   #echo -e "${InPullReqAuthr},\n ${InEmailMessage}. See below for more details on the Pull Request\n\n  \t Pull Request #              : ${InPullReqno} \n \t Pull Request from           : ${InPullReqFrom} \n \t Pull Request To             : ${InPullReqTo} \n \t Pull Request Create Date    : ${InPullReqCreateDt}\n \t Pull Request Participants   : ${InPullReqPrtcpts} \n\n Please resolve the issue as soon as possible." |  mailx -s "ACTION REQUIRED : ${InEmailMessage}" -c pasulav@michigan.gov pasulav@michigan.gov

}

function Validate_Error() {
  ErrorCd=$1
  ErrorMsg1=$2
  ErrorMsg2=$3
  ErrorMsg3=$4
  if [ $ErrorCd -ne 0 ]; then
      echo " "
      echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
      echo "!                                                                                                                                        "
      echo "! Error encountered"
      echo "! $ErrorMsg1"
      echo "! $ErrorMsg2"
      echo "! $ErrorMsg3"
      echo "! Exiting with error code $ErrorCd"
      echo "!                                                                                                                                        "
      echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
      exit $ErrorCd
 fi
}

#-------------------------------------------------------------------------------------------
# Script Start Point
#-------------------------------------------------------------------------------------------
Uid=`whoami`

CheckPullRequestsOutputFile=/tmp/${Uid}'_CheckPullRequests.log'
StopCheckPullRequestFile=/tmp/${Uid}'_StopCheckPullRequests.txt'
ScriptRunlog=/tmp/${Uid}'_CheckPullRequestsRun.log'
ScriptStartDateTime=`date +%m/%d/%Y-%I:%M:%S:%p`

echo "----------------------------------------------------------------------- "  >> $ScriptRunlog
echo "User ${Uid} started $0 at ${ScriptStartDateTime} with the following $# argument(s)"    >> $ScriptRunlog
if [ "$#" -lt 1 ]; then
   Validate_Error 1  "Number of parameters are NOT correct , Rerun the script with the following  parameters" "<BaseDir>" 
fi

BaseDir=$1
echo "BaseDir                   : $BaseDir"  >> $ScriptRunlog
echo " "  >> $ScriptRunlog

PropsDir=${BaseDir}/props
DependencyPropsFile=${PropsDir}/dependency.properties

if [ ! -f $DependencyPropsFile ];then
   Validate_Error 1 "Dependency Properties file [ $DependencyPropsFile ] does not exist "
fi

CherrypickUser=`grep  ^cherrypickUser $DependencyPropsFile  | cut -d '=' -f 2`
CherrypickEncPass=`grep  ^cherrypickEncPass $DependencyPropsFile  | cut -d '=' -f 2-`
BitbucketUrl=`grep ^bitbucketUrl $DependencyPropsFile | cut -d '=' -f 2`
BitBucketProject=`grep ^bitbucketProject $DependencyPropsFile | cut -d '=' -f 2`
BitBucketRepo=`grep ^bitbucketRepo $DependencyPropsFile | cut -d '=' -f 2`

if [ -z $CherrypickUser ];then
   Validate_Error 1  "cherrypickUser is blank in $DependencyPropsFile"
fi

if [ -z $CherrypickEncPass ];then
   Validate_Error 1 "cherrypickEncPass is blank in $DependencyPropsFile"
fi

if [ -z $BitbucketUrl ];then
   Validate_Error 1 "bitbucketUrl is blank in $DependencyPropsFile"
fi

if [ -z $BitBucketProject ];then
   Validate_Error 1 "bitbucketProject is blank in $DependencyPropsFile"
fi

if [ -z $BitBucketRepo ];then
   Validate_Error 1 "bitbucketRepo is blank in $DependencyPropsFile"
fi

echo "------------------------------------------------"    >> $ScriptRunlog
echo "Properties from $DependencyPropsFile"                >> $ScriptRunlog
echo "CherrypickUser              : ${CherrypickUser}"     >> $ScriptRunlog
echo "CherrypickEncPass           : CherrypickEncPass"     >> $ScriptRunlog
echo "BitbucketUrl                : ${BitbucketUrl}"       >> $ScriptRunlog 
echo "BitBucketProject            : ${BitBucketProject}"   >> $ScriptRunlog
echo "BitBucketRepo               : ${BitBucketRepo}"      >> $ScriptRunlog
echo "------------------------------------------------"    >> $ScriptRunlog

BitbucketUrl='https://'${BitbucketUrl}

echo "https BitbucketUrl          : ${BitbucketUrl}"                >> $ScriptRunlog
echo "CheckPullRequestsOutputFile : ${CheckPullRequestsOutputFile}" >> $ScriptRunlog
echo "StopCheckPullRequestFile    : ${StopCheckPullRequestFile}"    >> $ScriptRunlog
echo "CheckPullRequestsOutputFile : ${CheckPullRequestsOutputFile}" >> $ScriptRunlog
echo "ScriptRunlog                : ${ScriptRunlog}"                >> $ScriptRunlog

echo "CheckPullRequestsOutputFile : ${CheckPullRequestsOutputFile}"
echo "StopCheckPullRequestFile    : ${StopCheckPullRequestFile}"
echo "CheckPullRequestsOutputFile : ${CheckPullRequestsOutputFile}"
echo "ScriptRunlog                : ${ScriptRunlog}"

echo ""  >> $ScriptRunlog
echo "Starting loop"  >> $ScriptRunlog

while : 
do
    CurDateTime=`date +%m/%d/%Y-%I:%M:%S:%p`
    echo ""  >> $ScriptRunlog
	echo "  ${CurDateTime}" >> $ScriptRunlog
	rm -f ${CheckPullRequestsOutputFile}
	echo "  Running curl command to get all Open Pull Requests" >> $ScriptRunlog
	while [ $finished = 'no' ]; do
		CurlRes=$(curl -u ${CherrypickUser}:`echo ${CherrypickEncPass} | openssl enc -base64 -d`  -o ${CheckPullRequestsOutputFile} -write-out -w "%{http_code}\n" --silent  ${BitbucketUrl}/rest/api/latest/projects/${BitBucketProject}/repos/${BitBucketRepo}/pull-requests?state=open&start=$JsonStartPos)
		echo "  Curl Response Code        : ${CurlRes}" >> $ScriptRunlog
		JsonRespSize=`jq '.size' ${CheckPullRequestsOutputFile}`
		echo "  Open Pull Requests Counts : ${JsonRespSize}" >> $ScriptRunlog		
		JsonRespNextPageStart=`jq '.nextPageStart' ${CheckPullRequestsOutputFile}`
		JsonRespIsLastPage=`jq '.isLastPage' ${CheckPullRequestsOutputFile}`
		JsonRespNextPageStart=`jq '.nextPageStart' ${CheckPullRequestsOutputFile}`
		if [ $JsonRespNextPageStart != "null" ]; then
			JsonStartPos=$JsonRespNextPageStart
		fi
		for ((PrId=0;PrId<${JsonRespSize};PrId++))
		do
			PullReqId=`jq  .values[$PrId].id  ${CheckPullRequestsOutputFile} | sed 's/"//g'`
			PullReqTitle=`jq  .values[$PrId].title  ${CheckPullRequestsOutputFile} | sed 's/"//g'`
			PullReqState=`jq  .values[$PrId].state  ${CheckPullRequestsOutputFile} | sed 's/"//g'`
			PullReqCreatedDateEpoch=`jq  .values[$PrId].createdDate  ${CheckPullRequestsOutputFile} | sed 's/"//g'`
			PullReqFrom=`jq  .values[$PrId].fromRef.displayId  ${CheckPullRequestsOutputFile} | sed 's/"//g'`
			PullReqTo=`jq  .values[$PrId].toRef.displayId  ${CheckPullRequestsOutputFile} | sed 's/"//g'`			
			PullReqAuthor=`jq  .values[$PrId].author.user.displayName  ${CheckPullRequestsOutputFile} | sed 's/"//g' | cut -d' ' -f1,2 | cut -d',' -f2`			
			PullReqAuthorEmailId=`jq  .values[$PrId].author.user.emailAddress  ${CheckPullRequestsOutputFile} | sed 's/"//g'`
			PullReqPrtcptsTemp=`jq -c .values[$PrId].participants[].user.name ${CheckPullRequestsOutputFile} | sed 's/"//g'`
			PullReqPrtcptsEmailIdsTemp=`jq .values[$PrId].participants[].user.emailAddress ${CheckPullRequestsOutputFile} | sed 's/"//g'`
			PullReqOutcome=`jq .values[$PrId].properties.mergeResult.outcome ${CheckPullRequestsOutputFile} | sed 's/"//g'`			
			CurDtmEpoc=`date +%s`
			PullReqCreatedDateEpoch=`echo ${PullReqCreatedDateEpoch} | cut -c1-10`
            DiffInSecs=$((CurDtmEpoc-PullReqCreatedDateEpoch))
            DiffInMins=$(( DiffInSecs / 60 ))
			DiffInHrs=$(( DiffInMins / 60 ))
			PREpochDatetoDate=`date -d @${PullReqCreatedDateEpoch}`
			PullReqDisplayDate=`date -d "${PREpochDatetoDate}" +"%m/%d/%Y %I:%M"`
			echo "  Processing Pull Request $PullReqId" >> $ScriptRunlog
			if [ ! ${PullReqTo} = 'Mod1' ]; then
			    EmailMessage=''
                PullReqHasIssueSw='N'				
			    if [ ${PullReqOutcome} = "CONFLICTED" ];then
			        EmailMessage="Pull Request [${PullReqId}] is in ${PullReqOutcome} State"
			        PullReqHasIssueSw='Y'
			    elif [ ${PullReqOutcome} = "CLEAN" ] && [ ${DiffInMins} -gt 60 ];then
				     EmailMessage="Pull Request [${PullReqId}] is in ${PullReqState} State since ${PullReqDisplayDate}"					
                     PullReqHasIssueSw='Y'				
			    elif [ "${PullReqTitle}" = "Automatic merge failure" ];then
			   	     EmailMessage="Pull Request [${PullReqId}] has ${PullReqTitle} error"					
				     PullReqHasIssueSw='Y'
			    fi				
			    if [ ${PullReqHasIssueSw} = "Y" ];then
				   PullReqPrtcptsEmailIds=''
				   for mailId in $(echo  ${PullReqPrtcptsEmailIdsTemp})
				   do
					   if [ ! ${mailId} = "null" ]; then
						   PullReqPrtcptsEmailIds="${PullReqPrtcptsEmailIds},${mailId}"
					   fi
				   done
				   PullReqPrtcpts=''
				   for Prtcpt in $(echo ${PullReqPrtcptsTemp})
				   do
					  if [ ! ${Prtcpt} = "null" ]; then
						 PullReqPrtcpts="${PullReqPrtcpts},${Prtcpt}"
					  fi
				   done
				   PullReqPrtcptsEmailIds=${PullReqPrtcptsEmailIds#?}
				   PullReqPrtcpts=${PullReqPrtcpts#?}
				   echo "     Pull Req title                       :  ${PullReqTitle}" >> $ScriptRunlog
				   echo "     Pull Req from                        :  ${PullReqFrom}"  >> $ScriptRunlog
				   echo "     Pull Req to                          :  ${PullReqTo}"    >> $ScriptRunlog
				   echo "     Pull Req state                       :  ${PullReqState}" >> $ScriptRunlog
				   echo "     Pull Req author                      :  ${PullReqAuthor}" >> $ScriptRunlog
				   echo "     Pull Req author Email Id             :  ${PullReqAuthorEmailId}" >> $ScriptRunlog
				   echo "     Pull Req Participants                :  ${PullReqPrtcpts}" >> $ScriptRunlog
				   echo "     Pull Req Participants Email Ids      :  ${PullReqPrtcptsEmailIds}" >> $ScriptRunlog
				   echo "     Pull Req Outcome                     :  ${PullReqOutcome}" >> $ScriptRunlog
			       echo "     Pull Req Create Date                 :  ${PullReqDisplayDate}" >> $ScriptRunlog
			       echo "     Pull Request : ${PullReqId} has issues.Sending Email" >> $ScriptRunlog					
			       Send_Email ${PullReqId} "${PullReqAuthor}" ${PullReqFrom} ${PullReqTo} ${PullReqAuthorEmailId} "${PullReqPrtcptsEmailIds}" "${EmailMessage}" "${PullReqDisplayDate}" "${PullReqPrtcpts}" "${PullReqDisplayDate}" "${PullReqPrtcpts}"
			       echo " " >> $ScriptRunlog
			    fi # ${PullReqHasIssueSw} = "Y"
			fi # ${PullReqTo} = 'Mod1'
		done
		if [ $JsonRespIsLastPage = 'true' ]; then
			finished='yes'
		fi
	done
	finished='no'
	if [  -f ${StopCheckPullRequestFile} ]; then
	   rm -f ${StopCheckPullRequestFile}
	   echo "  Found ${StopCheckPullRequestFile} , stopping $0" >> $ScriptRunlog   
	   exit 0
	fi
	sleep 600 
done
